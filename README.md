Camilo Andrés Rojas Quevedo - 30000048017

-El botón "A" inicia o detiene la canción

-De las flechas, la punta derecha del todo adelanta la canción 10 segundos, y la punta izquierda del todo atrasa
10 segundos.

-La flecha superior e inferior no realizan ninguna acción.

-El botón "B" no realiza ninguna acción.

-Se puede ver la barra de progreso de la canción.

![image info](/assets/gb.png)