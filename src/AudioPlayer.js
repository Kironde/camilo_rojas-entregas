class AudioPlayer{

    constructor(domElement, c2) {
        this.isPaused = true;
        this.adelantar(c2);
        this.atrasar(c2);
        this.domElement = domElement;
        this.src = this.domElement.dataset.src;
        this.audio = new Audio(this.src);
        this.controls = {
            domElement: this.domElement.querySelector(".controls")
        };
        this.progress = this.domElement.querySelector(".cover .progress");
        this.initControls();

        this.audio.ontimeupdate = () => { this.updateUI(); }
    }

    initControls() {
        this.controls.play = this.controls.domElement.querySelector(".playBtn");
        if (this.controls.play) {
            this.initPlay(this.controls.play);
        }
    }
    
    initPlay(domElement) {
        domElement.onclick = () => {
            if (!this.isPaused) {
                this.isPaused=true;
                this.pause();
            } else {
                this.isPaused=false;
                this.play();
            }
        }
    }

    setCurrentTime(progress) {
        this.audio.currentTime = this.audio.duration * progress;
    }

    updateUI() {
        console.log("Updating UI");
        const total = this.audio.duration;
        const current = this.audio.currentTime;
        const progress = (current / total) * 100;
        this.progress.style.width = `${progress}%`;
    }

    play() {
        this.audio.play().then().catch(err => console.log(`Error al reproducir el archivo: ${err}`));
    }

    pause() {
        this.audio.pause();
    }

    adelantar(c2){
        c2.adelantar.onclick= () => {
            this.audio.currentTime+=10;
        }
    }

    atrasar(c2){

        c2.atrasar.onclick= () => {
            this.audio.currentTime-=10;
        }
    }
    
}